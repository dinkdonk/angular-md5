var gulp = require('gulp');
var jshint = require('gulp-jshint');

var paths = {
	scripts: 'app/scripts/**/*.js',
	styles: ['app/styles/**/*.scss', 'app/styles/**/*.css'],
	images: 'app/assets/images/**/*'
};

gulp.task('lint', function() {
	gulp.src([
		'./angular-md5.js'
	])
	.pipe(jshint())
	.pipe(jshint.reporter('default'));
});

gulp.task('default', ['lint']);